package homework.battle2.battle;

import java.util.Scanner;

public class ConsoleReader {
    Scanner skl = new Scanner(System.in);

    public Coordinate getCoordinate() {
        System.out.print("Произведите выстрел: ");
        String shot = skl.nextLine();
        shot = shot.substring(0, 1).toUpperCase() + shot.substring(1);
        char letter = shot.charAt(0);
        int x = Character.getNumericValue(letter);
        x = letter - 'A';
        int y = Integer.parseInt(shot.substring(1));
        Coordinate coordinate = new Coordinate(x, y);
//        System.out.println("Выстрел: " + " " + x + " " + y);
        return coordinate;
    }
}
