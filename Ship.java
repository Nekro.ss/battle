package homework.battle2.battle;

import java.util.ArrayList;
import java.util.List;

public class Ship {
    List<Deck> decks;

    Ship(int size, Coordinate coordinate, boolean isHorizontal) {
        decks = new ArrayList<>(size);
        for (int i = 0; i < size; i++) {
            Coordinate current;
            if (isHorizontal) {
                current = new Coordinate(coordinate.x + i, coordinate.y);
            } else {
                current = new Coordinate(coordinate.x, coordinate.y + i);
            }
            decks.add(new Deck(current));
        }
    }

    public boolean isUnderAttack(Shot shot) {
        for (int i = 0; i < decks.size(); i++) {
            if (shot.coordinate.equals(decks.get(i).coordinate)) {
                return true;
            }
        }
        return false;
    }

    public boolean attack(Shot shot) {
        for (int i = 0; i < decks.size(); i++) {
            if (shot.coordinate.equals(decks.get(i).coordinate)) {
                decks.get(i).isAlive = false;
                return true;
            }
        }
        return false;
    }


    private boolean isAlive() {
        for (int i = 0; i < decks.size(); i++) {
            if (decks.get(i).isAlive) {
                return true;
            }
        }
        return false;
    }

    private void print() {
        System.out.println("  A B C D E F G H I J");
        for (int i = 1; i < 11; i++) {
            System.out.print(i);
            nested:
            for (int j = 1; j < 11; j++) {
                for (Deck deck : decks) {
                    if (deck.coordinate.x == j && deck.coordinate.y == i) {
                        System.out.print(" +");
                        continue nested;
                    }
                }
                System.out.print(" .");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Ship ship = new Ship(3, new Coordinate(2, 2), true);
        Ship ship1 = new Ship(1, new Coordinate(8, 9), true);
        Shot shot1 = new Shot(2, new Coordinate(8, 9));
        ship1.isAlive();
        ship.print();
        ship1.print();
        System.out.println(ship1.attack(shot1));
        System.out.println(ship1.isAlive());
        ConsoleReader reader = new ConsoleReader();
        Coordinate coordinate = reader.getCoordinate();
        System.out.println("x" + coordinate.x + " " + "y" + coordinate.y);

    }
}


